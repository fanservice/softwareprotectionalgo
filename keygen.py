import sys
import os


def get_progression_fib(i: int) -> int:
    """Calculate the Fibonacci number.
    i=0 -> fib=1
    i=1 -> fib=2
    i=2 -> fib=3
    i=3 -> fib=5
    i=4 -> fib=8
    i=5 -> fib=13
    """
    first = 0
    second = 1
    res = 1
    for j in range(i+1):
        res = first + second
        first = second
        second = res
    return res


def combine_char_multi(char_user: str, char_key: str) -> str:
    """Multiply the ASCII-values of the characters.
    """
    res = ord(char_user)*ord(char_key)
    return res


def generate(user: str):
    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    key = "TheSecretStaticKey"
    length = 10
    license = ""
    for i in range(length):
        # Calculate which character to take next (the key is always one ahead)
        progression_user = get_progression_fib(i)
        progression_key = get_progression_fib(i+1)
        # Get the character from the user and the key
        char_user = user[progression_user % len(user)]
        char_key = key[progression_key % len(key)]
        # Calculate the next character based on the user and key character
        license_char = alphabet[combine_char_multi(
            char_user, char_key) % len(alphabet)]
        # Append the character to the license
        license += license_char

    return license


if __name__ == "__main__":
    # Check if there is one and only one argument.
    if len(sys.argv) != 2:
        print("Program only takes 1 argument.")
        os._exit(-1)

    user = sys.argv[1]
    # Check if the user name is at least 3 chars long
    if len(user) < 3:
        print("User has to be at least 3 chars long.")
        os._exit(-1)

    license = generate(user)
    print(f"The license key for the user '{user}' is '{license}'")
