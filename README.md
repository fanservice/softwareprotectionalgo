# Assignment 1
By: Kevin Wilhelm 11723947

# Introduction
In this assignment we have a look at obfuscation.
We will apply different obfuscation techniques with tigress.
Further we analyze the performance  and obfuscation impact to the application and code.
Finally, we are using static code analysis tools to have a look at the differences in the compiled code.
You can find the code, scripts etc at https://gitlab.com/fanservice/softwareprotectionalgo
Although the repo is public, please let me know if something went wrong. kevin.wilhelm@outlook.at, a11723947@unet.univie.ac.at

# Implementation and obfuscations
In this assignment we are going to have a look at the different obfuscation algorithms.
To create such, we are going to use tigress which helps us to create the obfuscated code.
As in the assignment, we recreated the python license key generator script in c.
Which can be found at moodle or in our git repo.
The script takes input from the command line, where input>3 and generate a licence for the given input.
The algorithm for the key is found in the assignment which is also uploaded to git.
To confirm that the implementation works, we attached the outcome of the algorithms.

In this assignment we are applying following obfuscations methods which are also uploaded to git:
default=main.c
Data encoding=encode.c
Opaque predicate=opaque.c
CFG flattening=flatten.c 
Virtualization=virt.c
Data encoding+Opaque predicate+CFG flattening

In our obfuscation examples we focus on the ```generate``` function.
We chose ```generate``` as ```generate``` is the most complex function in our licence generator.
Further, generate holds the key ```TheSecretStaticKey``` and a major part of the licence algorithm logic.
We want to obfuscate it to make it harder for the attacker.
If there are noticeable changes inside other function, we will discuss it too.
Of course normally you wouldn't upload your key to any public server or write it in your assignment.
Further the function ```generate``` contains integers, conditions, strings and output parameters.
We will see how those parameters will change.
For better code readability, please use gitlab.
## Settings for tigress
For tigress we are using the default settings as in the script below.
For better readability please have a lok at tigress.sh inside git.
The script is also a setup for tigress env. itself.
Further we are using the default settings for each of the algorithms.
For encode data we are using the generate function and the variables progression_user,progression_key are encoded.
```sh
export TIGRESS_HOME=~/tigress/3.1
export PATH=$PATH:~/tigress/3.1

tigress --Environment=x86_64:Linux:Gcc:4.6 --Transform=Virtualize --Functions=main,get_progression_fib,combine_char_multi,generate --out=virt.c main.c
tigress --Environment=x86_64:Linux:Gcc:4.6 --Transform=InitOpaque --Functions=main,get_progression_fib,combine_char_multi,generate --out=opaque.c main.c

tigress --Environment=x86_64:Linux:Gcc:4.6 --Transform=EncodeData \
  --LocalVariables='generate:progression_user,progression_key' \
    --EncodeDataCodecs='poly1' \
  --out=encode.c main.c

tigress --Environment=x86_64:Linux:Gcc:4.6 --Transform=Flatten --Functions=main,get_progression_fib,combine_char_multi,generate --out=flatten.c main.c

tigress --Environment=x86_64:Linux:Gcc:4.6 --Transform=EncodeData --Functions=main,get_progression_fib,combine_char_multi,generate \
  --LocalVariables='generate:progression_user,progression_key' \
  --EncodeDataCodecs='poly1' \
  --Transform=Flatten --Functions=main,get_progression_fib,combine_char_multi,generate \
  --Transform=InitOpaque --Functions=main,get_progression_fib,combine_char_multi,generate \
  --out=multi.c main.c

```
## Original Code
Here you can see the original code of generate with no obfuscation.
We will compare this function with he obfuscated code
```c
void generate(char *user, char license[]) {
    char *alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    char *key = "TheSecretStaticKey";

    uint8_t length = 10;

    for (int i = 0; i < length; ++i) {
        int progression_user = get_progression_fib(i);
        int progression_key = get_progression_fib(i + 1);

        char char_user = user[progression_user % strlen(user)];
        char char_key = key[progression_key % strlen(key)];
        char license_char = alphabet[(combine_char_multi(char_user, char_key) % strlen(alphabet))];
        license[i] = license_char;
    }
}

```
Similar to the python script we are using Alphanumeric characters to display the licence in the end.
Further we have a secret key which should be probably not TheSecretStaticKey, but it helps us find out what happens when we apply obfuscation.
We are using the fibonacci algorithm to generate numbers for the ```combine_char_multi``` function.
The ```combine_char_multi``` function takes two arguments which are basically two characters.
Those characters are combined by multiplication the char/ascii values of our key and console input, also known as user.

## Data Encoding
Data encoding changes the representation of integer types, to make it harder what value it contains.
For data encoding we are using tigress default codec which is poly1.
This means that our integers are converted to linear transformation equation similar to a*x+b.
We apply it on the int variables progression_user, progression_key.
We see inside the code that the integers of the function has change but the top secret key is still visible.
```c
    char_user = *(user + (unsigned long )((int )(1509654933U * progression_user - 2053070707U)) % tmp___1);
    tmp___2 = strlen((char const   *)key);
    char_key = *(key + (unsigned long )((int )(1509654933U * progression_key - 2053070707U)) % tmp___2);
```
```c
  alphabet = (char *)"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  key = (char *)"TheSecretStaticKey";
```
In the code example we see that tigress applied the linear transformation ```1509654933U * progression_key - 2053070707U```
Hence, it is harder for us to read, what the algorithm should achieve.
More over tigress replaced other variables with different(temporary) names.
```
    tmp___3 = combine_char_multi(char_user, char_key);
    tmp___4 = strlen((char const   *)alphabet);
    license_char = *(alphabet + (unsigned long )tmp___3 % tmp___4);
```
In the orginal code, tmp___3 and tmp___4 are directly used without further parameters. 
With the help of data encoding, we can obfuscate our algorithm which generates the licence.
Unfortunately  not the precious key as we applied it to the integers of the method.
The performance shouldn't change much, as linear transformations are a trivial task for the cpu.

## Opaque Predicate

With opaque predicate we add conditions and complexity to code which makes it harder to understand the algorithm and know which path we will take, although there is only correct.
This makes it harder for the attacker, as the attacker doesn't know which direction the code will go when it's executed by reading the code.
In our example we are using  lists and arrays with the default size of 30 for the opaque structs.

Inside the method we can already see the added linked lists with different outcomes. 
So the attacker doesn't know which can be replaced which are necessary and which are not.
Which means if the attacker removes a wrong statement the program won't start.

```c
  _1_generate__BEGIN_1 = 1;
  p18 = (struct _1_generate_1_opaque_NodeStruct *)malloc(sizeof(struct _1_generate_1_opaque_NodeStruct ));
  p18->next = p18;
  p18->prev = p18;
  _1_generate_1_opaque_list_1 = p18;
  r21 = rand();
  p22 = (struct _1_generate_1_opaque_NodeStruct *)malloc(sizeof(struct _1_generate_1_opaque_NodeStruct ));
  p22->data = 0 * r21;
  p22->next = _1_generate_1_opaque_list_1->next;
  p22->prev = _1_generate_1_opaque_list_1;
  (_1_generate_1_opaque_list_1->next)->prev = p22;
  _1_generate_1_opaque_list_1->next = p22;
  r19 = rand();
  p20 = (struct _1_generate_1_opaque_NodeStruct *)malloc(sizeof(struct _1_generate_1_opaque_NodeStruct ));
  p20->data = 1 * r19;
  p20->next = _1_generate_1_opaque_list_1->next;
  p20->prev = _1_generate_1_opaque_list_1;
  (_1_generate_1_opaque_list_1->next)->prev = p20;
  _1_generate_1_opaque_list_1->next = p20;
  _1_generate_1_opaque_ptr_1 = _1_generate_1_opaque_list_1->next;
  _1_generate_1_opaque_ptr_2 = _1_generate_1_opaque_ptr_1;
  _1_generate__END_1 = 1;
  _1_generate__BARRIER_2 = 1;
  alphabet = (char *)"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  key = (char *)"TheSecretStaticKey";
  length = (uint8_t )10;
  i = 0;
```
Unfortunately inside the obfuscated code we can see our algorithm which generates the key has no further changes.
Hence, it is not hard to find out which code should be executed when you have the obfuscated source code.
Further, our top secret key is still visible.
The performance could decrease, as we add further data structures which have to be evaluated.

```c
  alphabet = (char *)"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  key = (char *)"TheSecretStaticKey";
  length = (uint8_t )10;
  i = 0;
  while (i < (int )length) {
    tmp = get_progression_fib(i);
    progression_user = tmp;
    tmp___0 = get_progression_fib(i + 1);
    progression_key = tmp___0;
    tmp___1 = strlen((char const   *)user);
    char_user = *(user + (unsigned long )progression_user % tmp___1);
    tmp___2 = strlen((char const   *)key);
    char_key = *(key + (unsigned long )progression_key % tmp___2);
    tmp___3 = combine_char_multi(char_user, char_key);
    tmp___4 = strlen((char const   *)alphabet);
    license_char = *(alphabet + (unsigned long )tmp___3 % tmp___4);
    *(license + i) = license_char;
    i ++;
  }
```

## Control Flow Flattening
In Control Flow Flattening (CFF) we add complexity with the help of further branching.
So when an attacker Disassembled Codes it and look at the control flow graph, it's harder to read.
Which means, the attacker doesn't easily  know which branch is taken next.
We are applying tigress CFF with switch as flatten dispatch.
This means a switch state replaces any other condition statements.
In our example we can  see that the our for loop is replaced with a while.
Further, the condition inside the loop are replaced with the switch statement.
```c
unsigned long _1_generate_next ;

  {
  {
  _1_generate_next = 1UL;
  }
  while (1) {
    switch (_1_generate_next) {
    case 4: ;
    if (i < (int )length) {
      {
      _1_generate_next = 2UL;
      }
    } else {
      {
      _1_generate_next = 3UL;
      }
    }
    break;
    case 1: 
    alphabet = (char *)"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    key = (char *)"TheSecretStaticKey";
    length = (uint8_t )10;
    i = 0;
    {
    _1_generate_next = 4UL;
    }
    break;
    case 3: ;
    return;
    break;
    case 2: 
    tmp = get_progression_fib(i);
    progression_user = tmp;
    tmp___0 = get_progression_fib(i + 1);
    progression_key = tmp___0;
    tmp___1 = strlen((char const   *)user);
    char_user = *(user + (unsigned long )progression_user % tmp___1);
    tmp___2 = strlen((char const   *)key);
    char_key = *(key + (unsigned long )progression_key % tmp___2);
    tmp___3 = combine_char_multi(char_user, char_key);
    tmp___4 = strlen((char const   *)alphabet);
    license_char = *(alphabet + (unsigned long )tmp___3 % tmp___4);
    *(license + i) = license_char;
    i ++;
    {
    _1_generate_next = 4UL;
    }
    break;
    }
  }
```
This code is already more complex than the original code before.
We still see the key and what symbols are probably used for the licence key.
However, in the obfuscation techniques before, we could clearly see how often the loop is called inside the obfuscated source code.
With CFF we have to dig deeper, we have to follow the branches and understand what they do.
Although we see that there is a length variable, we don't know exactly what it is for.
As mentioned before, it is not 100% clear what happens when.
So we disassemble the switch statement, and we can see that we jump first inside case 1 which is the initialization of our variables.
Reassign the switch condition to a new value and execute the "loop" which increases the counter i.
As we already know we increase it until it has the value 10. and jump out.
This would take longer if we didn't know exactly what happens.
The code should be as fast as the original code as we don't really change anything for the cpu.
Eventually the branching inside the while loop with the switch statement could decrease the performance a little bit.

## Virtualization
As tigress website states, virtualize transform functions to an interpreter of its own bytecode.
This means that the function has a virtual instruction set which includes its own virtual program counter and a virtual stack pointer and a list of instruction handlers for each virtual instruction.
This also explains why virtualize has the largest code base, about twice as big as the second-largest code base and more than 10x as the untouched code base. 
It's not only hard to read but also very frustrating to understand what really happens.
Hence, if you are not used to reverse engineer programs you will have a bad time.
Especially if you are not very keen to the programming language, even if you see the Disassembled Coded code.
Moreover, the strings are combined into one string.

```c
char const   *_1_generate_$strings  =    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789\000TheSecretStaticKey\000";
```
The only hint that those strings are seperated is the name and the zero padding.
Further, tigress implements functions which helps us to read the code.
As in code segments, where the switch statement and union structures help us to get structure in this code.
Those helpers will be removed after the code is compiled.
```c
enum _1_generate_$op {
    _1_generate__call$func_LIT_0 = 191,
    _1_generate__store_int$right_STA_0$left_STA_1 = 152,
    _1_generate__store_unsigned_long$left_STA_0$right_STA_1 = 153,
    _1_generate__store_char$left_STA_0$right_STA_1 = 139,
    _1_generate__branchIfTrue$expr_STA_0$label_LAB_0 = 198,
    }
```
```c
union _1_generate_$node {
char _char ;
unsigned int _unsigned_int ;
unsigned char _unsigned_char ;
long _long ;
unsigned long _unsigned_long ;
void *_void_star ;
unsigned short _unsigned_short ;
unsigned long long _unsigned_long_long ;
signed char _signed_char ;
long long _long_long ;
int _int ;
short _short ;
};
```
We can also see the instruction tree to generate the random instruction set.
Each time the code is compiled, we get a new random dispatch of the program.
```c
void generate(char *user , char *license ) 
{ 
  char _1_generate_$locals[169] ;
  union _1_generate_$node _1_generate_$stack[1][32] ;
  union _1_generate_$node *_1_generate_$sp[1] ;
  unsigned char *_1_generate_$pc[1] ;

  {
  _1_generate_$sp[0] = _1_generate_$stack[0];
  _1_generate_$pc[0] = _1_generate_$array[0];
  while (1) {
    switch (*(_1_generate_$pc[0])) {
    case _1_generate__convert_unsigned_long2unsigned_long$left_STA_0$result_STA_0: 
    (_1_generate_$pc[0]) ++;
    (_1_generate_$sp[0] + 0)->_unsigned_long = (_1_generate_$sp[0] + 0)->_unsigned_long;
    break;
    case _1_generate__Lt_int_int2int$right_STA_0$result_STA_0$left_STA_1: 
    (_1_generate_$pc[0]) ++;
    (_1_generate_$sp[0] + -1)->_int = (_1_generate_$sp[0] + -1)->_int < (_1_generate_$sp[0] + 0)->_int;
    (_1_generate_$sp[0]) --;
    break;
    case _1_generate__string$result_STA_0$value_LIT_0: 
    (_1_generate_$pc[0]) ++;
    (_1_generate_$sp[0] + 1)->_void_star = (void *)(_1_generate_$strings + *((int *)_1_generate_$pc[0]));
    (_1_generate_$sp[0]) ++;
    _1_generate_$pc[0] += 4;
    break;
```
Not only is the codebase much larger but also more complex to read and understand.
The complexity and the underlying calls of different instruction, we would guess that the program takes longer to finish.

## Encode Data + CF Flatten + Opaque Predicate
For the combination of different techniques we are using CF Flattening, Data Encoding and Opaque predicates.
First we start with data encoding, follwed by cf flattening and in the end opaque predicates.
Similar to encode data we can already see the effects of the linear transformation.
```c
    tmp = get_progression_fib(i);
    progression_user = 975225277U * tmp + 1002384103U;
    tmp___0 = get_progression_fib(i + 1);
    progression_key = 975225277U * tmp___0 + 1002384103U;
    tmp___1 = strlen((char const   *)user);
```
As already mentioned this prevents to guess the correct value for this variable, which helps us to obfuscate our code.
In our tigress config we are using the same values and variables for encode data
Hence, this is the reason that only progression_user and progression_key are transformed.

Further, we can see that the cf flattening took place at the loop and the loop condition is replaced with a while and switch.
Hence, we can already guess that the condition flow graph will be more complex than the original one.
We can already see the combination of the encoding and cf flattening in the code example below.
```c
while (1) {
    switch (_2_generate_next) {
    case 4: ;
    if (i < (int )length) {
      _2_generate_next = 2UL;
    } else {
      _2_generate_next = 3UL;
    }
    break;
    case 1: 
    alphabet = (char *)"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    key = (char *)"TheSecretStaticKey";
    length = (uint8_t )10;
    i = 0;
    _2_generate_next = 4UL;
    break;
    case 3: ;
    return;
    break;
    case 2: 
    tmp = get_progression_fib(i);
    progression_user = 975225277U * tmp + 1002384103U;
    tmp___0 = get_progression_fib(i + 1);
    progression_key = 975225277U * tmp___0 + 1002384103U;
    tmp___1 = strlen((char const   *)user);
    char_user = *(user + (unsigned long )((int )(1509654933U * progression_user - 2053070707U)) % tmp___1);
    tmp___2 = strlen((char const   *)key);
    char_key = *(key + (unsigned long )((int )(1509654933U * progression_key - 2053070707U)) % tmp___2);
    tmp___3 = combine_char_multi(char_user, char_key);
    tmp___4 = strlen((char const   *)alphabet);
    license_char = *(alphabet + (unsigned long )tmp___3 % tmp___4);
    *(license + i) = license_char;
    i ++;
    _2_generate_next = 4UL;
    break;
```
Moreover, we applied the opaque predicate which should mislead the attacker to a wrong path.
We can see inside the source code of our combined methods, that tigress created our lists and structs to achieve the opaque predicate.
```c
struct _3_generate_1_opaque_NodeStruct *p19 ;
  int r20 ;
  struct _3_generate_1_opaque_NodeStruct *p21 ;
  int r22 ;
  struct _3_generate_1_opaque_NodeStruct *p23 ;
  int _3_generate__BEGIN_3 ;
  int _3_generate__END_3 ;
  int _3_generate__BARRIER_4 ;

  {
  _3_generate__BEGIN_3 = 1;
  p19 = (struct _3_generate_1_opaque_NodeStruct *)malloc(sizeof(struct _3_generate_1_opaque_NodeStruct ));
  p19->next = p19;
  p19->prev = p19;
```
As in the standalone example of the opaque predicate we can see the new generated lists and the different nodes.
The uncompiled code is still readable, or at least we know the major part of the code and of course we can still find text and find our keys.
Although I'm not sure how much impact the methods have, as it's a small example, I would still say that the code runs a little slower.


#Reverse Engineering
## Without any obfuscation
### Disassembled Coded Code
The unobfuscated code is easy to read.
We have a lot of assignments for the different variables and also the jumps and conditional jumps.

```asm
                           .text:00001341 53                               push   %rbx
                           .text:00001342 48 83 ec 38                      sub    $0x38,%rsp
                           .text:00001346 48 89 7d c8                      mov    %rdi,-0x38(%rbp)
                           .text:0000134a 48 89 75 c0                      mov    %rsi,-0x40(%rbp)
                           .text:0000134e 48 8d 05 0b 0d 00 00             lea    0xd0b(%rip),%rax        # 0x00002060
                           .text:00001355 48 89 45 e0                      mov    %rax,-0x20(%rbp)
                           .text:00001359 48 8d 05 3f 0d 00 00             lea    0xd3f(%rip),%rax        # 0x0000209f
                           .text:00001360 48 89 45 e8                      mov    %rax,-0x18(%rbp)
                           .text:00001364 c6 45 d0 0a                      movb   $0xa,-0x30(%rbp)
```
Basically this is just some assigment for variables and registers.
```asm

```
In the next step we see the jump to the loop.
```asm
.text:00001453 39 45 d4                         cmp    %eax,-0x2c(%rbp)
.text:0000136f e9 db 00 00 00                   jmpq   0x0000144f
```
Further, we iterate over the function as we know it and depending on the condition we jump out.
```asm
.text:00001456 0f 8c 18 ff ff ff                jl     0x00001374
```
### CFG
![Basic CFG](./media/cfg/main.png)
The control flow graph (cfg) of our generate function shows that the flow is very simple.
We have the start of our function, which goes to the loop condition on the bottom and checks if we have meat the loop condition.
### Strings
We can also find our secret strings very easily inside the hex view.
````asm
.rodata: 	00002060 		41 	42 	43 	44 	45 	46 	47 	48 		49 	4a 	4b 	4c 	4d 	4e 	4f 	50 		A 	B 	C 	D 	E 	F 	G 	H 	I 	J 	K 	L 	M 	N 	O 	P
.rodata: 	00002070 		51 	52 	53 	54 	55 	56 	57 	58 		59 	5a 	61 	62 	63 	64 	65 	66 		Q 	R 	S 	T 	U 	V 	W 	X 	Y 	Z 	a 	b 	c 	d 	e 	f
.rodata: 	00002080 		67 	68 	69 	6a 	6b 	6c 	6d 	6e 		6f 	70 	71 	72 	73 	74 	75 	76 		g 	h 	i 	j 	k 	l 	m 	n 	o 	p 	q 	r 	s 	t 	u 	v
.rodata: 	00002090 		77 	78 	79 	7a 	30 	31 	32 	33 		34 	35 	36 	37 	38 	39 	00 	54 		w 	x 	y 	z 	0 	1 	2 	3 	4 	5 	6 	7 	8 	9 	. 	T
.rodata: 	000020a0 		68 	65 	53 	65 	63 	72 	65 	74 		53 	74 	61 	74 	69 	63 	4b 	65 		h 	e 	S 	e 	c 	r 	e 	t 	S 	t 	a 	t 	i 	c 	K 	e 
````
## Data Encode
###Disassembled Code
For encode the function calls and loop condition are pretty much the same, we can also still see the function calls.
```asm
        .text:000013a6 c7 45 c0 00 00 00 00             movl   $0x0,-0x40(%rbp)
                           .text:000013ad e9 01 01 00 00                   jmpq   0x000014b3
                           .text:000013b2 8b 45 c0                         mov    -0x40(%rbp),%eax
                           .text:000013b5 89 c7                            mov    %eax,%edi
                           .text:000013b7 e8 ed fd ff ff                   callq  0x000011a9 <get_progression_fib>
```
But as we discussed earlier, in data encode we want to transform our integer to more complex variables.
Further, similar to the uncompleted code, we can se the encoding inside.
We know that this is the correct encoded line as we have the same numbers as in the Disassembled Coded version.
```asm
                           .text:000013c8 05 e7 2a bf 3b                   add    $0x3bbf2ae7,%eax
                           .text:000013cd 89 45 c8                         mov    %eax,-0x38(%rbp)
                           .text:000013d0 8b 45 c0                         mov    -0x40(%rbp),%eax
                           .text:000013d3 83 c0 01                         add    $0x1,%eax
                           .text:000013d6 89 c7                            mov    %eax,%edi
                           .text:000013d8 e8 cc fd ff ff                   callq  0x000011a9 <get_progression_fib>
                           .text:000013dd 89 45 cc                         mov    %eax,-0x34(%rbp)
                           .text:000013e0 8b 45 cc                         mov    -0x34(%rbp),%eax
                           .text:000013e3 69 c0 bd c1 20 3a                imul   $0x3a20c1bd,%eax,%eax
                           .text:000013e9 05 e7 2a bf 3b                   add    $0x3bbf2ae7,%eax
```
We got extra lines which are now more complicated to read for the attacker.
### CFG
The cfg looks similar to the code without obfuscation.
Same jumps but more lines because of the data encoding.
![Encoded CFG](./media/cfg/encode.png)


###Strings
Same as in the source code we just search for our key, fortunately we called our secret key, secret key.
```
.rodata: 	00002060 		41 	42 	43 	44 	45 	46 	47 	48 		49 	4a 	4b 	4c 	4d 	4e 	4f 	50 		A 	B 	C 	D 	E 	F 	G 	H 	I 	J 	K 	L 	M 	N 	O 	P
.rodata: 	00002070 		51 	52 	53 	54 	55 	56 	57 	58 		59 	5a 	61 	62 	63 	64 	65 	66 		Q 	R 	S 	T 	U 	V 	W 	X 	Y 	Z 	a 	b 	c 	d 	e 	f
.rodata: 	00002080 		67 	68 	69 	6a 	6b 	6c 	6d 	6e 		6f 	70 	71 	72 	73 	74 	75 	76 		g 	h 	i 	j 	k 	l 	m 	n 	o 	p 	q 	r 	s 	t 	u 	v
.rodata: 	00002090 		77 	78 	79 	7a 	30 	31 	32 	33 		34 	35 	36 	37 	38 	39 	00 	54 		w 	x 	y 	z 	0 	1 	2 	3 	4 	5 	6 	7 	8 	9 	. 	T
.rodata: 	000020a0 		68 	65 	53 	65 	63 	72 	65 	74 		53 	74 	61 	74 	69 	63 	4b 	65 		h 	e 	S 	e 	c 	r 	e 	t 	S 	t 	a 	t 	i 	c 	K 	e
```

## Flatten
Similar to our unobfuscated code, we can still see the function, names and so on.
We can also see that the counter for the loop stayed the same ```0xa``` which is 10.
```asm
                           .text:0000122e c6 45 b4 0a                      movb   $0xa,-0x4c(%rbp)
                           .text:00001232 c7 45 b8 00 00 00 00             movl   $0x0,-0x48(%rbp)
                           .text:00001239 48 c7 45 e0 04 00 00 00          movq   $0x4,-0x20(%rbp)
                           .text:00001241 e9 de 00 00 00                   jmpq   0x00001324
                           .text:00001246 8b 45 b8                         mov    -0x48(%rbp),%eax
                           .text:00001249 89 c7                            mov    %eax,%edi
                           .text:0000124b e8 dc 00 00 00                   callq  0x0000132c <get_progression_fib>
```
But as we already discussed, the complexity of our jumps increased.
Contrary to the orginal code we have over 10 jumps and conditions instead of the 2.
In the code example we can see the different jumps, ja,je, jmp and the cmpq for the conditions.
```asm
 .text:000011ca 74 29                            je     0x000011f5
                           .text:000011cc 48 83 7d e0 04                   cmpq   $0x4,-0x20(%rbp)
                           .text:000011d1 77 f2                            ja     0x000011c5
                           .text:000011d3 48 83 7d e0 03                   cmpq   $0x3,-0x20(%rbp)
                           .text:000011d8 0f 84 4b 01 00 00                je     0x00001329
                           .text:000011de 48 83 7d e0 03                   cmpq   $0x3,-0x20(%rbp)
                           .text:000011e3 77 e0                            ja     0x000011c5
                           .text:000011e5 48 83 7d e0 01                   cmpq   $0x1,-0x20(%rbp)
                           .text:000011ea 74 2c                            je     0x00001218
                           .text:000011ec 48 83 7d e0 02                   cmpq   $0x2,-0x20(%rbp)
                           .text:000011f1 74 53                            je     0x00001246
                           .text:000011f3 eb d0                            jmp    0x000011c5
                           .text:000011f5 0f b6 45 b4                      movzbl -0x4c(%rbp),%eax
                           .text:000011f9 39 45 b8                         cmp    %eax,-0x48(%rbp)
                           .text:000011fc 7d 0d                            jge    0x0000120b
                           .text:000011fe 48 c7 45 e0 02 00 00 00          movq   $0x2,-0x20(%rbp)
                           .text:00001206 e9 19 01 00 00                   jmpq   0x00001324
                           .text:0000120b 48 c7 45 e0 03 00 00 00          movq   $0x3,-0x20(%rbp)
                           .text:00001213 e9 0c 01 00 00                   jmpq   0x00001324
```
### CFG
We can clearly see the graph has multiple and more branches than the original one.
But we can still see the major loop branch.
An attacker,  would try to get inside this branch.
We can follow each condition and try to find out which one leads to the correct one.
So the graph can help us to understand what happens when we have multiple paths.
![Flatten CFG different branches](./media/cfg/flatten_part_1.png)
![Flatten CFG major branch](./media/cfg/flatten_part_2.png)
### Strings

Same as in the mentioned algorithms before, we can find the key with a text search.
````asm
.rodata: 	00002000 		01 	00 	02 	00 	00 	00 	00 	00 		41 	42 	43 	44 	45 	46 	47 	48 		. 	. 	. 	. 	. 	. 	. 	. 	A 	B 	C 	D 	E 	F 	G 	H
.rodata: 	00002010 		49 	4a 	4b 	4c 	4d 	4e 	4f 	50 		51 	52 	53 	54 	55 	56 	57 	58 		I 	J 	K 	L 	M 	N 	O 	P 	Q 	R 	S 	T 	U 	V 	W 	X
.rodata: 	00002020 		59 	5a 	61 	62 	63 	64 	65 	66 		67 	68 	69 	6a 	6b 	6c 	6d 	6e 		Y 	Z 	a 	b 	c 	d 	e 	f 	g 	h 	i 	j 	k 	l 	m 	n
.rodata: 	00002030 		6f 	70 	71 	72 	73 	74 	75 	76 		77 	78 	79 	7a 	30 	31 	32 	33 		o 	p 	q 	r 	s 	t 	u 	v 	w 	x 	y 	z 	0 	1 	2 	3
.rodata: 	00002040 		34 	35 	36 	37 	38 	39 	00 	54 		68 	65 	53 	65 	63 	72 	65 	74 		4 	5 	6 	7 	8 	9 	. 	T 	h 	e 	S 	e 	c 	r 	e 	t
.rodata: 	00002050 		53 	74 	61 	74 	69 	63 	4b 	65 		79 	00 	00 	00 	00 	00 	00 	00 		S 	t 	a 	t 	i 	c 	K 	e 	y 	. 	. 	. 	. 	. 	. 	. 
````

## Opaque 
### Disassembled Code
In the Disassembled Coded code we can see that the lists which are created for the opaque predicate is visible.
As an attacker, we can now assume that the software is obfuscated, but it is also an indicator what we are looking for.
The opaque predicate should make it harder to find out, which branch is the correct one.
In our case we have instead of 6 call(q) 15 call(q).
This can confuse the attacker to take a wrong path.
```asm
                           .text:00001651 48 89 05 30 2a 00 00             mov    %rax,0x2a30(%rip)        # 0x00004088 <_1_generate_1_opaque_list_1>
                           .text:00001658 b8 00 00 00 00                   mov    $0x0,%eax
                           .text:0000165d e8 8e fa ff ff                   callq  0x000010f0
                           .text:00001662 89 45 84                         mov    %eax,-0x7c(%rbp)
                           .text:00001665 bf 18 00 00 00                   mov    $0x18,%edi
                           .text:0000166a e8 71 fa ff ff                   callq  0x000010e0
                           .text:0000166f 48 89 45 b8                      mov    %rax,-0x48(%rbp)
                           .text:00001673 48 8b 45 b8                      mov    -0x48(%rbp),%rax
                           .text:00001677 c7 00 00 00 00 00                movl   $0x0,(%rax)
                           .text:0000167d 48 8b 05 04 2a 00 00             mov    0x2a04(%rip),%rax        # 0x00004088 <_1_generate_1_opaque_list_1>
                           .text:00001684 48 8b 50 08                      mov    0x8(%rax),%rdx
                           .text:00001688 48 8b 45 b8                      mov    -0x48(%rbp),%rax
                           .text:0000168c 48 89 50 08                      mov    %rdx,0x8(%rax)
                           .text:00001690 48 8b 15 f1 29 00 00             mov    0x29f1(%rip),%rdx        # 0x00004088 <_1_generate_1_opaque_list_1>
```
### CFG
The CFG shows that we still have the same paths as in the original one.
Hence, we can say that either the function was not suitable for the opaque predicate algorithm, or one iteration was not enough.
![Opaque](./media/cfg/opque.png)
### Strings
As in the algorithms before, we can just extract our secret key!
```asm
.rodata: 	00002060 		41 	42 	43 	44 	45 	46 	47 	48 		49 	4a 	4b 	4c 	4d 	4e 	4f 	50 		A 	B 	C 	D 	E 	F 	G 	H 	I 	J 	K 	L 	M 	N 	O 	P
.rodata: 	00002070 		51 	52 	53 	54 	55 	56 	57 	58 		59 	5a 	61 	62 	63 	64 	65 	66 		Q 	R 	S 	T 	U 	V 	W 	X 	Y 	Z 	a 	b 	c 	d 	e 	f
.rodata: 	00002080 		67 	68 	69 	6a 	6b 	6c 	6d 	6e 		6f 	70 	71 	72 	73 	74 	75 	76 		g 	h 	i 	j 	k 	l 	m 	n 	o 	p 	q 	r 	s 	t 	u 	v
.rodata: 	00002090 		77 	78 	79 	7a 	30 	31 	32 	33 		34 	35 	36 	37 	38 	39 	00 	54 		w 	x 	y 	z 	0 	1 	2 	3 	4 	5 	6 	7 	8 	9 	. 	T
.rodata: 	000020a0 		68 	65 	53 	65 	63 	72 	65 	74 		53 	74 	61 	74 	69 	63 	4b 	65 		h 	e 	S 	e 	c 	r 	e 	t 	S 	t 	a 	t 	i 	c 	K 	e
.rodata: 	000020b0 		79 	00 	00 	00 	00 	00 	00 	00 		00 	00 	00 	00 	80 	84 	2e 	41 		y 	
```
## Virtualize
### Disassembled Code
The Disassembled  code is huge, complex and hard to understand.
We introduce more jumps more calls and conversion like cltq which converts long to quads datatype. Which is not found in our original code.
```
                           .text:00001d53 48 8b 85 28 fe ff ff             mov    -0x1d8(%rbp),%rax
                           .text:00001d5a 8b 00                            mov    (%rax),%eax
                           .text:00001d5c 48 98                            cltq   
                           .text:00001d5e 48 01 d0                         add    %rdx,%rax
                           .text:00001d61 48 89 85 28 fe ff ff             mov    %rax,-0x1d8(%rbp)
```
We got over 40 jumps inside the virtualized code base and it is hard to understand what happens. 

Hence, as a software owner I would recommend this method or a combination to secure important parts of my code.

### CFG
Similar to the Disassembled Coded code, the CFG is huge, complex and hard to understand.

![Virtual CFG](./media/cfg/Virtual.png)
### String
Unfortunately our top secret key is still not protected.
If we think of a secret to generate a token or something similar, virtual wouldn't protect us.
```
.rodata: 	00003010 		41 	42 	43 	44 	45 	46 	47 	48 		49 	4a 	4b 	4c 	4d 	4e 	4f 	50 		A 	B 	C 	D 	E 	F 	G 	H 	I 	J 	K 	L 	M 	N 	O 	P
.rodata: 	00003020 		51 	52 	53 	54 	55 	56 	57 	58 		59 	5a 	61 	62 	63 	64 	65 	66 		Q 	R 	S 	T 	U 	V 	W 	X 	Y 	Z 	a 	b 	c 	d 	e 	f
.rodata: 	00003030 		67 	68 	69 	6a 	6b 	6c 	6d 	6e 		6f 	70 	71 	72 	73 	74 	75 	76 		g 	h 	i 	j 	k 	l 	m 	n 	o 	p 	q 	r 	s 	t 	u 	v
.rodata: 	00003040 		77 	78 	79 	7a 	30 	31 	32 	33 		34 	35 	36 	37 	38 	39 	00 	54 		w 	x 	y 	z 	0 	1 	2 	3 	4 	5 	6 	7 	8 	9 	. 	T
.rodata: 	00003050 		68 	65 	53 	65 	63 	72 	65 	74 		53 	74 	61 	74 	69 	63 	4b 	65 		h 	e 	S 	e 	c 	r 	e 	t 	S 	t 	a 	t 	i 	c 	K 	e
.rodata: 	00003060 		79 	00 	00 	00 	00 	00 	00 	00 		50 	72 	6f 	67 	72 	61 	6d 	6d 		y
```

## Encode Data + CF Flatten + Opaque Predicate 
### Disassembled Code
Similar to the stand alone of encode data we can see that our integer variable has a larger value to confuse an attacker.
As before, I only know this because I was looking at the original combined code to know which number I'm looking for.
```asm
                           .text:00001b7d 8b 45 a4                         mov    -0x5c(%rbp),%eax
                           .text:00001b80 69 c0 bd c1 20 3a                imul   $0x3a20c1bd,%eax,%eax
                           .text:00001b86 05 e7 2a bf 3b                   add    $0x3bbf2ae7,%eax
                           .text:00001b8b 89 45 a8                         mov    %eax,-0x58(%rbp)
                           .text:00001b8e 8b 45 8c                         mov    -0x74(%rbp),%eax
```
Further, we can se the multiple jumps from the cf flattening.
We got multiple comparison for the jump condition and more than 10 jumps.
```asm
                           .text:00001aef 48 83 7d c8 04                   cmpq   $0x4,-0x38(%rbp)
                           .text:00001af4 74 29                            je     0x00001b1f
                           .text:00001af6 48 83 7d c8 04                   cmpq   $0x4,-0x38(%rbp)
                           .text:00001afb 77 f2                            ja     0x00001aef
                           .text:00001afd 48 83 7d c8 03                   cmpq   $0x3,-0x38(%rbp)
                           .text:00001b02 0f 84 80 01 00 00                je     0x00001c88
                           .text:00001b08 48 83 7d c8 03                   cmpq   $0x3,-0x38(%rbp)
                           .text:00001b0d 77 e0                            ja     0x00001aef
                           .text:00001b0f 48 83 7d c8 01                   cmpq   $0x1,-0x38(%rbp)
                           .text:00001b14 74 2c                            je     0x00001b42
                           .text:00001b16 48 83 7d c8 02                   cmpq   $0x2,-0x38(%rbp)
                           .text:00001b1b 74 53                            je     0x00001b70
```
We also found our function calls and the representation of the opaque lists which are used for the opaque predicate.
```asm
.text:00001a0d 48 8b 05 6c 26 00 00             mov    0x266c(%rip),%rax        # 0x00004080 <_3_generate_1_opaque_list_1>
                           .text:00001a14 48 8b 50 08                      mov    0x8(%rax),%rdx
                           .text:00001a18 48 8b 45 d8                      mov    -0x28(%rbp),%rax
                           .text:00001a1c 48 89 50 08                      mov    %rdx,0x8(%rax)
                           .text:00001a20 48 8b 15 59 26 00 00             mov    0x2659(%rip),%rdx        # 0x00004080 <_3_generate_1_opaque_list_1>
                           .text:00001a27 48 8b 45 d8                      mov    -0x28(%rbp),%rax
                           .text:00001a2b 48 89 50 10                      mov    %rdx,0x10(%rax)
                           .text:00001a2f 48 8b 05 4a 26 00 00             mov    0x264a(%rip),%rax        # 0x00004080 <_3_generate_1_opaque_list_1>
                           .text:00001a36 48 8b 40 08                      mov    0x8(%rax),%rax
                           .text:00001a3a 48 8b 55 d8                      mov    -0x28(%rbp),%rdx
                           .text:00001a3e 48 89 50 10                      mov    %rdx,0x10(%rax)
                           .text:00001a42 48 8b 05 37 26 00 00             mov    0x2637(%rip),%rax        # 0x00004080 <_3_generate_1_opaque_list_1>
```
Although we know that the defender generated opaque predicates, we still need to find them and bypass them.

### CFG
In our CFG we can see the effects of the cf flattening as in the stand-alone example.
We got a bigger CFG comparing to the original code.
![Combined Methods](./media/cfg/combinition.png)
Same as in the cf flatten before we see that we have more conditions than the original code.

### String
As in all the other examples we could find our top secret key.



# Performance and Code Data Ratio

| Name                  | Original                                     | Encode                | Flatten               | Opaque                | Virtualize       | Combination           |
|-----------------------|----------------------------------------------|-----------------------|-----------------------|-----------------------|------------------|-----------------------|
| Perfomanced Guessed   | -                                            | Same                  | Same                  | Slower                | Slower           | Slower                |
| Performanced Messured | 0.000043 Seconds                             | 0.000046 Seconds      | 0.000044 Seconds      | 0.000037 Seconds      | 0.000065 Seconds | 0.000034 Seconds      |
| Code / Data Ratio     | 1061 bytes / 16 bytes                        | 1157 bytes / 16 bytes | 1477 bytes / 16 bytes | 2645 bytes / 16 bytes | 7301 bytes /  1288 bytes   | 3077 bytes / 16 bytes |
| Key                   | Your username is: kevin your key: h8pMzjhgzC |   h8pMzjhgzC                    | h8pMzjhgzC            |  h8pMzjhgzC                 |       h8pMzjhgzCDѨ�D�JV                   |        h8pMzjhgzC                     |

## Performance

We messured the perfomance by counting the clock cycles and use the average of 10 executions.
Further, we can see that flatten, encode and opaque take about the same time and virtualize is slower.
This is corresponds to our guess.
Only the combination which is faster than thought.
This means that the obfuscation has no big impact because our generator is trivial.
Further, the compiler enhance our program.
Hence, this needs further research.
For virtualize we guessed that it is slower.
This is probably due the fact that we create a abstract syntax tree, bytecode for the input function and a lot of control flow graphs.
This would be a draw back for to virtualize technique.

## Code Data Ratio
We see in our table that the code data ratio is especially for virtualizing and the combination of multiple techniques higher
Opaque has a larger code base, as we created more constructs for the opaque predicate.
The combination of our three techniques increases the size respective to the applied techniques which is predictable.
Virtualize technique increased code and data. 
We guess  that the responsibility for the large code size are dispatch units, list of instruction handlers.
We guess  that the responsibility for the larger data size are the byte arrays.
So our application is bigger when we use virtualize.

## Key Output
As we can see we get the same key in all obfuscation methods except for virtualize.
We couldn't find out why exactly the output has more characters than it should.
This could be bypassed by output the correct size of characters inside a loop, instead just printing the character array.

# Summary
In this assignment we saw the different obfuscation techniques and the reverse engineering to.
We showed the original and obfuscated source code.
Further, we could see that especially virtualize does a lot of obfuscation even to the uncompiled code.
The other obfuscation methods, did obfuscation but didn't make it *impossible* to read.
We could always find our precious key, in a productive system this could be fatal.
Moreover, we had a look at the binary and control flow graph.
In the assembly code we could see new function calls and values which obfuscate the code.
Further, in the control flow graph we could see the new branches which should mislead an attacker and make it harder to find out where the code exactly does.
Especially virtualize did a great job for obfuscate Disassembled code and the control flow graph.
We also saw that the virtualize code is bigger in code itself and in data.
Unfortunately we still find our top secret key.
It was very exciting to see what the obfuscation methods are doing and how the end result will be.
Nevertheless, when you know you code base it is much easier to search for specific changes, as you wrote the code and obfuscated it.
I would call this obfuscation bias, as you know exactly where the new parts are.
Especially if you are looking for your key.


## Sources
https://tigress.wtf/virtualize.html