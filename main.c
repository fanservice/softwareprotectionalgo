#include <stdint-gcc.h>
#include <time.h>
#include <malloc.h>
#include "stdio.h"
#include "string.h"

int combine_char_multi(char char_user, char char_key);

void generate(char *user, char license[]);

int get_progression_fib(int i);

int main(int argc, char **argv) {


    clock_t start, end;
    double cpu_time_used;

    start = clock();



    if (argc != 2) {
        printf("Programm takes only 1 argument");
        return -1;
    };

    if (strlen(argv[1]) < 3) {
        return -1;
    }
    char *user = argv[1];
    char license[10];
    printf("\nYour username is: %s your key: ", user);
    generate(user, license);
    printf("%s", license);
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("\n time spent: %f", cpu_time_used);
    return 0;
}

int get_progression_fib(int i) {
    int first = 0;
    int second = 1;
    int res = 1;
    for (int j = 0; j < i + 1; ++j) {
        res = first + second;
        first = second;
        second = res;
    }
    return res;
}

int combine_char_multi(char char_user, char char_key) {
    int res = char_user * char_key;
    return res;
}

void generate(char *user, char license[]) {
    char *alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    char *key = "TheSecretStaticKey";

    uint8_t length = 10;

    for (int i = 0; i < length; ++i) {
        int progression_user = get_progression_fib(i);
        int progression_key = get_progression_fib(i + 1);

        char char_user = user[progression_user % strlen(user)];
        char char_key = key[progression_key % strlen(key)];
        char license_char = alphabet[(combine_char_multi(char_user, char_key) % strlen(alphabet))];
        license[i] = license_char;
    }
}


