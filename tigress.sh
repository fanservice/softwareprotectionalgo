export TIGRESS_HOME=~/tigress/3.1
export PATH=$PATH:~/tigress/3.1

tigress --Environment=x86_64:Linux:Gcc:4.6 --Transform=Virtualize --Functions=main,get_progression_fib,combine_char_multi,generate --out=virt.c main.c
tigress --Environment=x86_64:Linux:Gcc:4.6 --Transform=InitOpaque --Functions=main,get_progression_fib,combine_char_multi,generate --out=opaque.c main.c

tigress --Environment=x86_64:Linux:Gcc:4.6 --Transform=EncodeData \
  --LocalVariables='generate:progression_user,progression_key' \
    --EncodeDataCodecs='poly1' \
  --out=encode.c main.c

tigress --Environment=x86_64:Linux:Gcc:4.6 --Transform=Flatten --Functions=main,get_progression_fib,combine_char_multi,generate --out=flatten.c main.c

tigress --Environment=x86_64:Linux:Gcc:4.6 --Transform=EncodeData --Functions=main,get_progression_fib,combine_char_multi,generate \
  --LocalVariables='generate:progression_user,progression_key' \
  --EncodeDataCodecs='poly1' \
  --Transform=Flatten --Functions=main,get_progression_fib,combine_char_multi,generate \
  --Transform=InitOpaque --Functions=main,get_progression_fib,combine_char_multi,generate \
  --out=multi.c main.c
